package com.six;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0 Jan 25, 2021
 **/

public class ForLoopTriangle {

	public static void main(String[] args) {

		for (int i = 1; i <= 9; i++) {
			for (int j = 9; j >= i; j--) {
				System.out.print(" ");
			}
			for (int j = 1; j <= i; j++) {
				System.out.print(i + " ");
			}
			System.out.println("");
		}

	}

}
